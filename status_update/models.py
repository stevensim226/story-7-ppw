from django.db import models

class StatusUpdate(models.Model):
    name = models.TextField(max_length=50)
    description = models.TextField(max_length=500)
    color_code = models.IntegerField(default=0)

    def __str__(self):
        return f"({self.name}) - {self.description[:20]}..."