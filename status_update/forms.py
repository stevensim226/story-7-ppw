from django import forms
from .models import StatusUpdate

class StatusUpdateForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "form-control",
        "id" : "name",
        "placeholder" : "John Doe"
    }))
    description = forms.CharField(widget=forms.Textarea(attrs={
        "class" : "form-control",
        "id" : "description",
        "rows" : 3,
        "placeholder" : "I feel happy today!"
    }))
    class Meta:
        model = StatusUpdate
        exclude = ["color_code"]