from django.test import TestCase, LiveServerTestCase, Client
from django.urls import reverse, resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from .models import StatusUpdate
from .views import list_view, confirm_view, add_view, change_color_view
from .forms import StatusUpdateForm

class TestForms(TestCase):
    def test_form_valid_data(self):
        form = StatusUpdateForm(data = {
            "name" : "Steven",
            "description" : "Hello World"
        })
        self.assertTrue(form.is_valid())

    def test_form_invalid_data(self):
        form = StatusUpdateForm(data = {})
        self.assertFalse(form.is_valid())

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.list_url = reverse("status_update:list_view")
        self.confirm_url = reverse("status_update:confirm_view")
        self.add_url = reverse("status_update:add_view")
        self.change_color_url = reverse("status_update:change_color")
        self.new_status = StatusUpdate.objects.create(
            name = "Hello", 
            description = "World"
        )

    def test_GET_list_url(self):
        response = self.client.get(self.list_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,"status_update/index.html")

    def test_POST_confirm_url(self):
        response = self.client.post(self.confirm_url, {
            "name" : "Steven",
            "description" : "1234567890"
        }, follow = True)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response,"1234567890")
        self.assertTemplateUsed(response,"status_update/confirm.html")

    def test_GET_confirm_url_redirects(self):
        response = self.client.get(self.confirm_url, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateNotUsed(response, "status_update/confirm.html")

    def test_GET_add_url_redirects(self):
        response = self.client.get(self.add_url)
        self.assertEquals(response.status_code, 302)

    def test_POST_add_url_creates(self):
        response = self.client.post(self.add_url, data = {
            "name" : "Bukit Cemara",
            "description" : "Sesuatu"
        })
        self.assertEquals(StatusUpdate.objects.last().name, "Bukit Cemara")
        self.assertEquals(response.status_code, 302)

    def test_POST_change_color_url_increments(self):
        response = self.client.post(self.change_color_url, {
            "idx" : self.new_status.id
        })
        new_status_updated = StatusUpdate.objects.get(name = "Hello", description = "World")
        self.assertEquals(new_status_updated.color_code, 1)

class TestUrls(TestCase):
    def setUp(self):
        self.list_url = reverse("status_update:list_view")
        self.confirm_url = reverse("status_update:confirm_view")
        self.add_url = reverse("status_update:add_view")
        self.change_color_url = reverse("status_update:change_color")

    def test_list_url__uses_correct_view(self):
        list_view_func = resolve(self.list_url)
        self.assertEquals(list_view_func.func, list_view)

    def test_confirm_url_uses_correct_view(self):
        confirm_view_func = resolve(self.confirm_url)
        self.assertEquals(confirm_view_func.func, confirm_view)

    def test_add_url_uses_correct_view(self):
        add_view_func = resolve(self.add_url)
        self.assertEquals(add_view_func.func, add_view)

    def test_change_color_url_uses_correct_view(self):
        change_color_view_func = resolve(self.change_color_url)
        self.assertEquals(change_color_view_func.func, change_color_view)

class TestModels(TestCase):
    def setUp(self):
        self.new_status = StatusUpdate.objects.create(name = "Steven", description = "Hello World!")

    def test_instance_created(self):
        self.assertEquals(StatusUpdate.objects.count(), 1)

    def test_instance_is_correct(self):
        self.assertEquals(StatusUpdate.objects.first().name, "Steven")

    def test_to_string(self):
        self.assertIn("(Steven) - Hello", str(self.new_status))

class FunctionalTesting(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080");
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
    
    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_create_new_status(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        self.assertIn("Status Update", selenium.page_source)

        wait = WebDriverWait(selenium, 3)
        new_status_button = wait.until(EC.element_to_be_clickable((By.ID, 'new_status')))

        new_status_button.send_keys(Keys.RETURN)

        name_input = wait.until(EC.element_to_be_clickable((By.ID, 'name')))
        description_input = wait.until(EC.element_to_be_clickable((By.ID, 'description')))

        name_input.send_keys("John Doe")
        description_input.send_keys("Lorem ipsum")
        post_button = selenium.find_element_by_class_name("btn-success")

        post_button.send_keys(Keys.RETURN)

        post_button_2 = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.btn.btn-success')))

        self.assertIn("John Doe", selenium.page_source)

        
        post_button_2.send_keys(Keys.RETURN)

        # Test change color
        # Click change color on a status then check if the color if the same as before.
        self.assertIn("John Doe", selenium.page_source)
        self.assertIn("Lorem ipsum", selenium.page_source)

        first_status_update_color_before = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, ".col-sm-12:nth-child(1) .rounded-sm"))).value_of_css_property("background-color")
        first_change_color_button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, ".col-sm-12:nth-child(1) .w-100"))).click()
        
        first_status_update_color_after = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, ".col-sm-12:nth-child(1) .rounded-sm"))).value_of_css_property("background-color")
        self.assertNotEquals(first_status_update_color_after, first_status_update_color_before)

    def test_cancel_post_status(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        self.assertIn("Status Update", selenium.page_source)

        wait = WebDriverWait(selenium, 3)
        new_status_button = wait.until(EC.element_to_be_clickable((By.ID, 'new_status')))
        
        new_status_button.send_keys(Keys.RETURN)

        name_input = wait.until(EC.element_to_be_clickable((By.ID, 'name')))
        description_input = wait.until(EC.element_to_be_clickable((By.ID, 'description')))

        name_input.send_keys("Cancelled")
        description_input.send_keys("Cancelled")

        post_button = selenium.find_element_by_class_name("btn-success")

        post_button.send_keys(Keys.RETURN)
        
        cancel_button = selenium.find_element_by_css_selector(".btn.btn-danger")

        cancel_button.send_keys(Keys.RETURN)

        self.assertNotIn("Cancelled",selenium.page_source)
        

