from django.urls import path
from . import views

app_name = "status_update"

urlpatterns = [
    path("", views.list_view, name="list_view"),
    path("confirm/", views.confirm_view, name="confirm_view"),
    path("add/", views.add_view, name="add_view"),
    path("change_color/", views.change_color_view , name="change_color")
]