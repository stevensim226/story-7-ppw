from django.apps import AppConfig


class StatusUpdateConfig(AppConfig):
    name = 'status_update'
