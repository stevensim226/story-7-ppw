from django.shortcuts import render, HttpResponse, redirect
from .models import StatusUpdate
from .forms import StatusUpdateForm

def list_view(request):
    status_update_query = [(status, status.color_code % 5) for status in StatusUpdate.objects.all()]
    status_update_query = sorted(status_update_query, key = lambda status: -status[0].id)
    context = {
        "status_update_query" : status_update_query,
        "page" : "list_view",
        "form" : StatusUpdateForm
    }
    return render(request, "status_update/index.html", context)

def confirm_view(request):
    if request.method == "POST":
        context = {
            "name" : request.POST["name"],
            "description" : request.POST["description"],
        }
        return render(request, "status_update/confirm.html", context)

    return redirect("status_update:list_view")

def add_view(request):
    if request.method == "POST":
        form = StatusUpdateForm(request.POST)
        if form.is_valid():
            status = form.save(commit=False)
            status.color_code = 0
            status.save()
    return redirect("status_update:list_view")

def change_color_view(request):
    if request.method == "POST":
        idx = request.POST["idx"]
        selected_status = StatusUpdate.objects.get(id=idx)
        selected_status.color_code += 1
        selected_status.save()
    return redirect("status_update:list_view")