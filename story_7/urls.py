from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path("", include("status_update.urls")),
    path("story-8/", include("mini_profile.urls")),
    path("story-9/", include("book_database.urls"))
]
