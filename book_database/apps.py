from django.apps import AppConfig


class BookDatabaseConfig(AppConfig):
    name = 'book_database'
