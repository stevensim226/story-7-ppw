from django.urls import path
from . import views

app_name = "book_database"

urlpatterns = [
    path("", views.book_homepage_view, name="homepage"),
    path("add_like", views.book_add_like, name="add_like"),
    path("top_five", views.top_five, name="top_five"),
    path("get_likes/<str:book_id>", views.get_likes, name="get_likes")
]