from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.core import serializers
from .models import Book
import json

def book_homepage_view(request):
    return render(request, "book_database/index.html")

def book_add_like(request):
    if request.method == "POST":
        book_id = request.POST["book_id"]
        try:
            selected_book = Book.objects.get(book_id = book_id)
            selected_book.likes += 1
            selected_book.save()
        except:
            Book.objects.create(
                title = request.POST["title"],
                book_id = request.POST["book_id"],
                cover_link = request.POST["cover_link"],
                description = request.POST["description"]
            )
    return redirect("book_database:homepage")

def top_five(request):
    return JsonResponse({"data" : list(Book.objects.order_by("-likes")[:5].values())})

def get_likes(request,book_id):
    try:
        selected_book = Book.objects.get(book_id = book_id)
        return JsonResponse({"likes" : selected_book.likes})
    except:
        return JsonResponse({"likes" : 0})