from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from .models import Book
from .views import book_homepage_view

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import time
import json

class TestBookDatabase(TestCase):
    def setUp(self):
        self.client = Client()
        self.sample_book = Book.objects.create(
            title = "Nichijou",
            book_id = "8_SiDAEACAAJ",
            cover_link = "http://books.google.com/books/content?id=8_SiDAEACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api",
            description = "description"
        )
        self.like_link_for_sample_book = reverse("book_database:add_like")
        self.homepage_link = reverse("book_database:homepage")
        self.top_five_link = reverse("book_database:top_five")

    def test_homepage_works(self):
        response = self.client.get(self.homepage_link)
        homepage_function = resolve(self.homepage_link).func
        self.assertEquals(book_homepage_view, homepage_function)
        self.assertEquals(response.status_code, 200)

    def test_book_is_added_to_like(self):
        self.assertEquals(Book.objects.count(), 1)

    def test_book_model_to_string(self):
        self.assertEquals("Nichijou - 8_SiDAEACAAJ", str(self.sample_book))

    def test_book_add_like(self):
        self.client.post(self.like_link_for_sample_book, data= {
            "title" : "Nichijou",
            "book_id" : "8_SiDAEACAAJ",
            "cover_link" : "http://books.google.com/books/content?id=8_SiDAEACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api",
            "description" : "description"
        })
        new_like_count = Book.objects.get(title = "Nichijou").likes
        self.assertEquals(new_like_count, 2)

    def test_book_like_new(self):
        self.client.post(self.like_link_for_sample_book, data= {
            "title" : "New Book",
            "book_id" : "12345",
            "cover_link" : "http://website.com/image.jpeg",
            "description" : "description2"
        })
        self.assertEquals(Book.objects.last().title, "New Book")
        self.assertEquals(Book.objects.last().likes, 1)

    def test_get_top_5_json(self):
        response = self.client.get(self.top_five_link)
        self.assertEquals(response.status_code, 200)

    def test_get_like_count(self):
        self.client.post(self.like_link_for_sample_book, data= {
            "title" : "New Book",
            "book_id" : "12345",
            "cover_link" : "http://website.com/image.jpeg",
            "description" : "description2"
        })
        self.client.post(self.like_link_for_sample_book, data= {
            "title" : "New Book",
            "book_id" : "12345",
            "cover_link" : "http://website.com/image.jpeg",
            "description" : "description2"
        })
        response = self.client.get(reverse("book_database:get_likes",args=["12345"]))
        self.assertEquals(json.loads(response.content)["likes"], 2)

class TestFunctionalityBookDatabase(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080");
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
    
    def tearDown(self):
        self.selenium.quit()
        super().tearDown()
    
    def test_find_book(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + "/story-9")
        wait = WebDriverWait(selenium, 5)

        book_form = wait.until(EC.element_to_be_clickable((By.ID, 'book-form')))
        book_form.send_keys("Nichijou")

        search_button = wait.until(EC.element_to_be_clickable((By.ID, 'find-btn')))
        search_button.click()   

        wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'card')))
        result_container = selenium.find_element_by_id("book-list")
        result_list = result_container.find_elements_by_class_name("card")
        self.assertEquals(len(result_list), 10)

        like_button = result_list[0].find_element_by_class_name("like-btn")
        for x in range(5):
            like_button.send_keys(Keys.RETURN)

        like_amount = result_list[0].find_elements_by_tag_name("h5")[1]
        self.assertNotEquals("Likes: 0", like_amount)