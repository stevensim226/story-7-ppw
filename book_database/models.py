from django.db import models

class Book(models.Model):
    book_id = models.CharField(unique = True, max_length = 100)
    title = models.CharField(max_length = 100)
    cover_link = models.URLField(unique = True)
    description = models.TextField()
    likes = models.IntegerField(default=1)

    def __str__(self):
        return "{} - {}".format(self.title, self.book_id)
