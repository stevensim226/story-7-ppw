$(document).ready(function(){

    $("#accordion").accordion();

    function sortByPosition(a, b) {
        var item1pos = Number($(a).attr("position"))
        var item2pos = Number($(b).attr("position"))
        
        if (item1pos < item2pos) return -1;
        if (item2pos > item2pos) return 1;
        return 0;
    }

    function sort_ul_up(clicked_h3, operation) {
        var element_list = $("#accordion").children()
        var ul = $("#accordion")

        var clicked_pos = element_list.index(clicked_h3)
        var clicked_div = element_list[clicked_pos+1]
        if (clicked_pos == 0 && operation == "up") return
        if (clicked_pos == element_list.length-1 && operation == "down") return

        

        if (operation == "up") {
            var above_h3 = $(element_list[clicked_pos-2])
            var above_div = $(element_list[clicked_pos-1])

            $(clicked_h3).attr("position", above_h3.attr("position")) //Ubah posisi yang di klik
            $(clicked_div).attr("position", above_div.attr("position"))

            above_h3.attr("position", above_h3.attr("position") + 1) //Turunkan posisi yang diatas
            above_div.attr("position", above_div.attr("position") + 1)
        
        } else if (operation == "down") {
            var below_h3 = $(element_list[clicked_pos+2])
            var below_div = $(element_list[clicked_pos+3])

            $(clicked_h3).attr("position", below_h3.attr("position")) //Ubah posisi yang di klik
            $(clicked_div).attr("position", below_div.attr("position"))

            below_h3.attr("position", below_h3.attr("position") - 1) //Turunkan posisi yang diatas
            below_div.attr("position", below_div.attr("position") - 1)
        }

        element_list.sort(sortByPosition)
       
        $.each(element_list, (index, li) => {
            ul.append(li);
        })
    }

    $(".up").click((e) => {
        var lix = e.target.parentElement
        console.log("up")
        sort_ul_up(lix,"up")
    })

    $(".down").click((e) => {
        var lix = e.target.parentElement
        sort_ul_up(lix,"down")
    })
    
    // Challenge Story 8
    var counter = 0
    $("#change-theme-btn").click(() => {
        var body_color
        var tab_footer_color

        switch (counter % 5) {
            case 0:
                body_color = "#3b6978"
                tab_footer_color = "#204051"
                break
            case 1:
                body_color = "#79bac1"
                tab_footer_color = "#2a7886"
                break
            case 2:
                body_color = "#7f78d2"
                tab_footer_color = "#481380"
                break
            case 3:
                body_color = "#4cbbb9"
                tab_footer_color = "#0779e4"
                break
            case 4:
                body_color = "#6c757d"
                tab_footer_color = "black"
                break
        }
        $("#main-bg").css("background-color", body_color)
        $(".accordion-tab").css("background-color", tab_footer_color)
        $(".navbar-footer-bg").css("background-color", tab_footer_color)
        $(".down").css("background-color", body_color)
        $(".up").css("background-color", body_color)
        counter++
    })

    
})