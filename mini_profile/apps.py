from django.apps import AppConfig


class MiniProfileConfig(AppConfig):
    name = 'mini_profile'
