from django.test import TestCase, LiveServerTestCase, Client
from django.urls import reverse, resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from mini_profile.views import mini_profile_view

# Test for Story 8
class TestUrls(TestCase):
    def setUp(self):
        self.mini_profile_url = reverse("mini_profile:mini_profile")

    def test_mini_profile_works(self):
        mini_profile_func = resolve(self.mini_profile_url)
        self.assertEquals(mini_profile_func.func, mini_profile_view)

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.mini_profile_url = reverse("mini_profile:mini_profile")

    def test_GET_mini_profile_url(self):
        response = self.client.get(self.mini_profile_url)
        self.assertTemplateUsed(response, "mini_profile/index.html")

class FunctionalTesting(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080");
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
    
    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_profile_hidden(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + "/story-8")

        # Check whether accordion hides data correctly.

        second_desc = selenium.find_elements_by_xpath("//*[contains(text(), 'I can write code in Python, Javascript, Java, and Ruby')]")[0]
        self.assertFalse(second_desc.is_displayed())

        wait = WebDriverWait(selenium, 3)
        accordion = wait.until(EC.element_to_be_clickable((By.ID, 'accordion')))
        second_tab = accordion.find_elements_by_tag_name("h3")[1]
        second_tab.send_keys(Keys.RETURN)
        self.assertTrue(second_desc.is_displayed())

    def test_profile_click_up(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + "/story-8")
        wait = WebDriverWait(selenium, 3)

        # Check if up button works
        up_button_lst = selenium.find_elements_by_class_name("up")
        second_up_button = up_button_lst[1]
        second_up_button.click()

        # Check if accordion's first h3 is second element
        
        accordion = wait.until(EC.element_to_be_clickable((By.ID, 'accordion')))
        first_tab = accordion.find_elements_by_tag_name("h3")[0]

        self.assertIn("What can I do?", first_tab.text)

    def test_profile_click_down(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + "/story-8")
        wait = WebDriverWait(selenium, 3)

        # Check if down button works
        down_button_lst = selenium.find_elements_by_class_name("down")
        first_down_button = down_button_lst[0]
        first_down_button.click()

        # Check if accordion's first h3 is second element (second tab is brought up to first)
        accordion = wait.until(EC.element_to_be_clickable((By.ID, 'accordion')))
        first_tab = accordion.find_elements_by_tag_name("h3")[0]
        self.assertIn("What can I do?", first_tab.text)

    def test_change_theme(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + "/story-8")
        wait = WebDriverWait(selenium, 3)


        background_color_old = selenium.find_element_by_id("main-bg").value_of_css_property("background-color")
        accordion_tab_color_old = selenium.find_element_by_class_name("accordion-tab").value_of_css_property("background-color")

        # Click change theme button
        change_theme_btn = wait.until(EC.element_to_be_clickable((By.ID, 'change-theme-btn')))
        change_theme_btn.click()

        background_color_new = selenium.find_element_by_id("main-bg").value_of_css_property("background-color")
        accordion_tab_color_new = selenium.find_element_by_class_name("accordion-tab").value_of_css_property("background-color")

        self.assertNotEquals(background_color_old, background_color_new)
        self.assertNotEquals(accordion_tab_color_old, accordion_tab_color_new)

