from django.urls import path
from . import views

app_name = "mini_profile"

urlpatterns = [
    path("", views.mini_profile_view, name="mini_profile")
]