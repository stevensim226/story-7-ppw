from django.shortcuts import render, HttpResponse

def mini_profile_view(request):
    return render(request, "mini_profile/index.html")